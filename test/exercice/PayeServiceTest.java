package exercice;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by lacouf on 2017-10-10.
 */
@RunWith(MockitoJUnitRunner.class)
public class PayeServiceTest {

    @Mock
    EmployeRepo employeRepo;

    @Mock
    PayeRepo payeRepo;

    @InjectMocks
    PayeService payeService;

    @Test
    public void testFaitPaye() {
        Employe e = new Employe();
        e.setNom("Raphael");
        e.setTauxHoraire(10);
        when(employeRepo.findEmploye("Raphael")).thenReturn(e);
        payeService.faitLaPaye("Raphael", 10);
        verify(employeRepo, times(2)).findEmploye(anyString());
    }
}
