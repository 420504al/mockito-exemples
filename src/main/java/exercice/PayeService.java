package exercice;

/**
 * Created by lacouf on 2017-10-10.
 */
public class PayeService {

    private EmployeRepo employeRepo;
    private PayeRepo payeRepo;

    public void faitLaPaye(String nomEmploye, double heures) {
        Employe employe = employeRepo.findEmploye((nomEmploye));
        double montant = employe.getTauxHoraire() * heures;
        Paye paye = new Paye(employe, montant);
        payeRepo.savePaye(paye);
    }

    public EmployeRepo getEmployeRepo() {
        return employeRepo;
    }

    public void setEmployeRepo(EmployeRepo employeRepo) {
        this.employeRepo = employeRepo;
    }

    public PayeRepo getPayeRepo() {
        return payeRepo;
    }

    public void setPayeRepo(PayeRepo payeRepo) {
        this.payeRepo = payeRepo;
    }
}
