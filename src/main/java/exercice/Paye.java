package exercice;

/**
 * Created by lacouf on 2017-10-10.
 */
public class Paye {
    public long id;
    public Employe employe;
    public double montant;

    public Paye(Employe employe, double montant) {
        this.employe = employe;
        this.montant = montant;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Employe getEmploye() {
        return employe;
    }

    public void setEmploye(Employe employe) {
        this.employe = employe;
    }

    public double getMontant() {
        return montant;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }
}
