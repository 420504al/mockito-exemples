package exercice;

/**
 * Created by lacouf on 2017-10-10.
 */
public interface PayeRepo {
    Paye findPaye(long id);
    void savePaye(Paye paye);
}
